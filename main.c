#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>

#define L 100 //Altezza del reticolo
#define D 100 //Lunghezza del reticolo
#define nmcs 500 //Numero di anni
#define nruns 1 //Iterazione del ciclo esterno
#define Sstart 800 //Numero iniziale di squali
#define Mstart 2500 //Numero iniziale di pesci
#define agereproductionmin 8 //Anni dopo i quali i pesci si riproducono
#define agereproductionshark 8 //Anni dopo i quali gli squali si riproducono
#define agedeathshark 4 //Anni dopo i quali gli squali muoiono se non mangiano

void initial(int world[L][D], int agemin[L][D], int ageshark[L][D], int noeat[L][D]);
int rndom(int, int);
void minnowmove(int world[L][D], int agemin[L][D], int ageshark[L][D], int noeat[L][D]);
void population(int world[L][D], int *, int *);
void stampa(int world[L][D]);
void gif(int world[L][D], FILE *, FILE *, FILE *);
void nuclear(int world[L][D], int agemin[L][D], int, int, int, int, double);

int main(){
    
    srand48(time(NULL));
    
    FILE *fn1;
    FILE *fn2;
    FILE *fn3;
    FILE *fn4;
    FILE *fn5;
    
    fn1=fopen("minnow.txt", "w");
    fn2=fopen("shark.txt", "w");
    fn3=fopen("water.txt", "w");
    fn4=fopen("pop.txt", "w");
    fn5=fopen("med.txt", "w");
    
    int i, k, nshark=0, nminnow=0;
    int world[L][D]={0}, agemin[L][D]={0}, ageshark[L][D]={0}, noeat[L][D]={0};
    int j, disastro, lMax=0, lMin=0, dMax=0, dMin=0, numpandemie=0;
    int tmax[1000]={0}, tmin[1000]={0}, lmin[1000]={0}, lmax[1000]={0}, dmin[1000]={0}, dmax[1000]={0};

    
    double sumnshark=0, sumnminnow=0, sum2nshark=0, sum2nminnow=0;
    double numshark[nmcs]={0}, numminnow[nmcs]={0};
    double Mortalita=0;
    double mortalita[100]={0};
    
    disastro=1; //Modalità pandemia attivata
//    disastro=0; //Modalità pandemia disattivata
    
    if(disastro==1) numpandemie=rndom(0, 100);

    for(k=1; k<=nruns; k++){ //Ciclo esterno per effettuare medie sulle popolazioni
        
        initial(world, agemin, ageshark, noeat); //Funzione di inizializzazione del sistema
        
        if(disastro==1){
              
              for(j=0; j<numpandemie; j++){
                  
                  tmin[j]=rndom(1, nmcs-1); //Tempo minimo del disastro
                  lmin[j]=rndom(1, L-1); //Margini minimi di effetto del disastro
                  dmin[j]=rndom(1, D-1);
                  mortalita[j]=(double)lrand48()/RAND_MAX; //Indice di mortalità del disastro
                  
                  do{
                      tmax[j]=rndom(tmin[j], nmcs); //Tempo massimo del disastro
                  }while(abs(tmax[j]-tmin[j])>100);
                  
                  do{
                      lmax[j]=rndom(lmin[j], L);
                  }while(abs(lmax[j]-lmin[j])>L);
                  
                  do{
                      dmax[j]=rndom(dmin[j], D);
                  }while(abs(dmax[j]-dmin[j])>D);
                  
                  printf("Numero disastro=%d Inizio disastro=%d Fine disastro=%d XmaxDisastro=%d XminDisastro=%d YmaxDisastro=%d YminDisastro=%d Mortalità=%f\n\n", j+1, tmin[j], tmax[j], lmax[j], lmin[j], dmax[j], dmin[j], mortalita[j]);
              }
              
          }
        
        for(i=0; i<nmcs; i++){ //Ciclo degli anni
            
            minnowmove(world, agemin, ageshark, noeat); //Cuore dell'implementazione
            
            if(disastro==1){
              
                 for(j=0; j<numpandemie; j++){ //Ciclo sui vari disastri
                     lMax=lmax[j]; //Indici dei vari disastri
                     lMin=lmin[j];
                     dMax=dmax[j];
                     dMin=dmin[j];
                     Mortalita=mortalita[j];
                     if(i>=tmin[j] && i<=tmax[j]) nuclear(world, agemin, lMax, lMin, dMax, dMin, Mortalita); //Azione dei disastri sulla popolazione
                 }
                 
             }
            
            population(world, &nshark, &nminnow); //Funzione di calcolo delle popolazioni
            gif(world, fn1, fn2, fn3); //Per la gif
            
            numshark[i]+=nshark; //Somma delle popolazioni all'i-simo anno
            numminnow[i]+=nminnow;
            
            nshark=0;
            nminnow=0;
        }
        
    }
    
//    fprintf(fn4, "0 %f %f %f %f\n", numshark[i]/nruns, numminnow[i]/nruns, (double)Sstart/(L*D), (double)Mstart/(L*D));
    for(i=0; i<nmcs; i++){ //Ciclo per il calcolo delle medie
        
        sumnshark+=numshark[i]/nruns;
        sumnminnow+=numminnow[i]/nruns;
        sum2nshark+=pow(numshark[i]/nruns,2);
        sum2nminnow+=pow(numminnow[i]/nruns,2);
        
        fprintf(fn4, "%d %f %f %f %f %f %f\n", i+1, numshark[i]/nruns, numminnow[i]/nruns, sumnshark/(i+1)/(L*D), sumnminnow/(i+1)/(L*D), (sum2nshark/(i+1))-((sumnshark/i+1)*(sumnshark/i+1)), (sum2nminnow/(i+1))-((sumnminnow/i+1)*(sumnminnow/i+1))); //I risultati sono riportati come densità di popolazioni
        
    }
    
    fclose(fn1);
    fclose(fn2);
    fclose(fn3);
    fclose(fn4);
    fclose(fn5);
    
    system("/opt/homebrew/bin/gnuplot comandi.txt");
//    system("/opt/homebrew/bin/gnuplot comandi1.txt"); //PER GIF
    
    return 0;
    
}

int rndom(int min, int max){ //Funzione per la generazioni di numeri casuali in un certo intervallo
    
    int rnd;
    
    return rnd=(int)(((double)lrand48()/RAND_MAX)*(max+1-min)+min);
    
}

void initial(int world[L][D], int agemin[L][D], int ageshark[L][D], int noeat[L][D]){ //Funzione di inizializzazione
    
    int i, x, y;
    
    for(i=0; i<Sstart; i++){ //Popolo il mondo di squali in modo casuale
        
        do{
            x=rndom(0,L-1);
            y=rndom(0,L-1);
        
        }while(world[x][y]==1 || world[x][y]==2);
        
        world[x][y]=2; //2 indica lo squalo
        
    }
    
    for(i=0; i<Mstart; i++){ //Popolo il mondo di pesci in modo casuale
        
        do{
            x=rndom(0,L-1);
            y=rndom(0,L-1);
        
        }while(world[x][y]==1 || world[x][y]==2);
        
        world[x][y]=1; //1 indica il pesce
        
    }
    
    for(x=0; x<L; x++){ //Ciclo di inizializzazione a zero
        for(y=0; y<D; y++){
            agemin[x][y]=0; //Matrice con l'età dei pesci
            ageshark[x][y]=0; //Matrice con l'età degli squali
            noeat[x][y]=0; //Matrice che tiene conto da quanti anni gli squali non mangiano
        }
    }
    
}

void minnowmove(int world[L][D], int agemin[L][D], int ageshark[L][D], int noeat[L][D]){ //Funzione di evoluzione
    
    int x, y, k, xnew, ynew, up, upmin, down, downmin, left, leftmin, right, rightmin, flag, rnd1, ao=0;
    int dx[4]={0}, dy[4]={0}, alreadymoved[L][D]={0}, alreadymovedshark[L][D]={0};
    
    dx[0]=1; //dx[] dy[] array per muoversi nelle quattro direzioni
    dx[1]=-1;
    dy[2]=1;
    dy[3]=-1;
    
    for(x=0; x<L; x++){ //Passo in rassegna tutto l'array world[][] riga per riga
        for(y=0; y<D; y++){
            
            if(world[x][y]==1 && alreadymoved[x][y]==0){ //Caso in cui incontro un pesce che non ho già mosso in precedenza
                
                down=0;
                up=0;
                right=0;
                left=0;
                flag=0;
                ao=0;
                
                for(k=0; k<4; k++){ //Ciclo sulle 4 direzioni
                    
                    xnew=x+dx[k];
                    if(xnew>L-1) xnew=0; //PBC
                    else if(xnew<0) xnew=L-1;
                    
                    ynew=y+dy[k];
                    if(ynew>D-1) ynew=0; //PBC
                    else if(ynew<0) ynew=D-1;
                    
                    if(world[xnew][ynew]==0 && k==0) down=1; //Casella sotto libera
                    else if(world[xnew][ynew]==0 && k==1) up=2; //Casella sopra libera
                    else if(world[xnew][ynew]==0 && k==2) right=3; //Casella a destra libera
                    else if(world[xnew][ynew]==0 && k==3) left=4; //Casella a sinistra libera
                    
                }
                
                if(down!=0 || up!=0 || right!=0 || left!=0){ //Se almeno una delle quattro possibili posizioni è libera
                    
                    do{
                        rnd1=rndom(1, 4); //Scelgo in modo casuale dove muovermi
                        
                        if(rnd1==down){
                            
                            if(agemin[x][y]==agereproductionmin){ //Se il pesce ha raggiunto l'età di riproduzione
                                world[x][y]=1; //La casella che lascio genera un pesce
                                agemin[x][y]=0; //Il nuovo pesce ha età nulla per ora, dato che il pesce che si è riprodotto ritorna ad avere un anno
                            }else world[x][y]=0; //Se il pescce non ha raggiunto età di riproduzione si sposta in modo classico
                            
                            if(x+1>L-1){ //PBC
                                world[0][y]=1; //Il pesce va ad occupare la nuova posizione
                                alreadymoved[0][y]=1; //Tengo conto del fatto che questo pesce l'ho già mosso in questo ciclo
                                agemin[0][y]=agemin[x][y]+1; //Vado ad aumentare l'età del pesce di un'unità
                            }
                            else if(x+1<0){
                                world[L-1][y]=1;
                                alreadymoved[L-1][y]=1;
                                agemin[L-1][y]=agemin[x][y]+1;
                            }
                            else{
                                world[x+1][y]=1;
                                alreadymoved[x+1][y]=1;
                                agemin[x+1][y]=agemin[x][y]+1;
                            }
                            
                            if(world[x][y]==1) agemin[x][y]=1; //Modifico l'età del nuovo pesce e la pongo pari ad un'unità
                            else agemin[x][y]=0; //Se non si è riprodotto azzero l'età nella casella dove si trovava prima il pesce
                            
                            flag=1; //Contatore per farmi uscire dal do while
                        } else if(rnd1==up){ //Stesse considerazioni per direzione verso l'alto
                            
                            if(agemin[x][y]==agereproductionmin){
                                world[x][y]=1;
                                agemin[x][y]=0;
                            }else world[x][y]=0;
                            
                            if(x-1>L-1){
                                world[0][y]=1;
                                alreadymoved[0][y]=1;
                                agemin[0][y]=agemin[x][y]+1;
                            }
                            else if(x-1<0){
                                world[L-1][y]=1;
                                alreadymoved[L-1][y]=1;
                                agemin[L-1][y]=agemin[x][y]+1;
                            }
                            else{
                                world[x-1][y]=1;
                                alreadymoved[x-1][y]=1;
                                agemin[x-1][y]=agemin[x][y]+1;
                            }
                            
                            if(world[x][y]==1) agemin[x][y]=1;
                            else agemin[x][y]=0;
                            
                            flag=1;
                        }else if(rnd1==right){ //Stesse considerazioni con direzione verso destra
                            
                            if(agemin[x][y]==agereproductionmin){
                                world[x][y]=1;
                                agemin[x][y]=0;
                            }else world[x][y]=0;
                            
                            if(y+1>D-1){
                                world[x][0]=1;
                                alreadymoved[x][0]=1;
                                agemin[x][0]=agemin[x][y]+1;
                            }
                            else if(y+1<0){
                                world[x][D-1]=1;
                                alreadymoved[x][D-1]=1;
                                agemin[x][D-1]=agemin[x][y]+1;
                            }
                            else{
                                world[x][y+1]=1;
                                alreadymoved[x][y+1]=1;
                                agemin[x][y+1]=agemin[x][y]+1;
                            }
                            
                            if(world[x][y]==1) agemin[x][y]=1;
                            else agemin[x][y]=0;
                            
                            flag=1;
                        }else if(rnd1==left){ //Stessa considerazioni con direzione verso sinistra
                            
                            if(agemin[x][y]==agereproductionmin){
                                world[x][y]=1;
                                agemin[x][y]=0;
                            }else world[x][y]=0;
                            
                            if(y-1>D-1){
                                world[x][0]=1;
                                alreadymoved[x][0]=1;
                                agemin[x][0]=agemin[x][y]+1;
                            }
                            else if(y-1<0){
                                world[x][D-1]=1;
                                alreadymoved[x][D-1]=1;
                                agemin[x][D-1]=agemin[x][y]+1;
                            }
                            else{
                                world[x][y-1]=1;
                                alreadymoved[x][y-1]=1;
                                agemin[x][y-1]=agemin[x][y]+1;
                            }
                            
                            if(world[x][y]==1) agemin[x][y]=1;
                            else agemin[x][y]=0;
                            
                            flag=1;
                        }
                        
                        ao++; //Contatore di controllo, per verificare che il ciclo do while non vada in loop
                        if(ao>200){
                            printf("FAIL %d\n", ao);
                            break;
                        }
                        
                        
                    }while(flag==0);
                    
//                    stampa(world);
//                    printf("agemin\n");
//                    stampa(agemin);
                    
                }else if(down==0 && up==0 && right==0 && left==0){ //Caso in cui tutte le posizioni adiacenti al pesce sono occupate
                    
                    agemin[x][y]=agemin[x][y]+1; //Il pesce non si sposta, ma la sua età aumenta comunque di un'unità. Nel caso in cui abbia raggiunto età di riproduzione e non abbia possibilità di movimento, il pesce non si riproduce per evitare sovrapopolazione
                    world[x][y]=1;
//                    stampa(world);
//                    printf("agemin\n");
//                    stampa(agemin);
                    
                }
                //Fine considerazioni su pesci
            }else if(world[x][y]==2 && alreadymovedshark[x][y]==0){ //Caso in cui incontro uno squalo che non ho già mosso in precedenza
                
                down=0;
                downmin=0;
                up=0;
                upmin=0;
                right=0;
                rightmin=0;
                left=0;
                leftmin=0;
                flag=0;
                ao=0;
                
                for(k=0; k<4; k++){ //Ciclo sullo spostamento nelle caselle adiacenti come per i pesci
                    
                    xnew=x+dx[k];
                    if(xnew>L-1) xnew=0; //PBC
                    else if(xnew<0) xnew=L-1;
                    
                    ynew=y+dy[k];
                    if(ynew>D-1) ynew=0; //PBC
                    else if(ynew<0) ynew=D-1;
                    
                    if(world[xnew][ynew]==0 && k==0) down=1; //Casella in basso libera
                    else if(world[xnew][ynew]==1 && k==0) downmin=5; //Casella in basso occupata da un pesce
                    else if(world[xnew][ynew]==0 && k==1) up=2; //Casella in alto libera
                    else if(world[xnew][ynew]==1 && k==1) upmin=6; //Casella in alto occupata da un pesce
                    else if(world[xnew][ynew]==0 && k==2) right=3; //Casella a destra libera
                    else if(world[xnew][ynew]==1 && k==2) rightmin=7; //Casella a destra occupata da un pesce
                    else if(world[xnew][ynew]==0 && k==3) left=4; //Casella a sinistra libera
                    else if(world[xnew][ynew]==1 && k==3) leftmin=8; //Casella a sinistra occupata da un pesce
                    
                }
                
                if(downmin!=0 || upmin!=0 || rightmin!=0 || leftmin!=0){ //Caso in cui almeno una delle quattro caselle è occupata da un pesce (Lo squalo tra spostarsi liberamente e spostarsi per mangiare predilige la seconda)
                    
                    do{
                        rnd1=rndom(5, 8); //Scelgo a caso quale pesce mangiare
                        
                        if(rnd1==downmin){
                            
                            if(ageshark[x][y]==agereproductionshark){ //Caso in cui lo squalo abbia raggiunto età di riproduzione
                                world[x][y]=2; //Nella casella che lascio si genera un nuovo squalo
                                ageshark[x][y]=0; //Età del nuovo squalo per ora nulla (utile per aggiornare età del genitore)
                                noeat[x][y]=0; //Il nuovo squalo ha zero anni di digiuno
                            }else world[x][y]=0; //Se non ha raggiunto età di riproduzione la casella che lascio è vuota
                            
                            if(x+1>L-1){ //PBC
                                world[0][y]=2; //Popolo la casella in cui mi sposto
                                alreadymovedshark[0][y]=1; //Registro che questo squalo è già stato mosso in questa iterazione
                                agemin[0][y]=0; //Annullo l'età del pesce che è stato mangiato
                                ageshark[0][y]=ageshark[x][y]+1; //Aumento di un'unità l'età dello squalo
                                noeat[0][y]=0; //Azzero gli anni di digiuno dello squalo
                            }
                            else if(x+1<0){ //PBC
                                world[L-1][y]=2;
                                alreadymovedshark[L-1][y]=1;
                                agemin[L-1][y]=0;
                                ageshark[L-1][y]=ageshark[x][y]+1;
                                noeat[L-1][y]=0;
                            }
                            else{ //PBC
                                world[x+1][y]=2;
                                alreadymovedshark[x+1][y]=1;
                                agemin[x+1][y]=0;
                                ageshark[x+1][y]=ageshark[x][y]+1;
                                noeat[x+1][y]=0;
                            }
                            
                            if(world[x][y]==2) ageshark[x][y]=1; //Se lo squalo si è riprodotto, il figlio ha un anno
                            else{
                                ageshark[x][y]=0; //Se non si è riprodotto azzero le quantità di interesse nella casella che ho lasciato
                                noeat[x][y]=0;
                            }
                            
                            flag=1; //Contatore per uscire dal ciclo do while
                        }else if(rnd1==upmin){ //Stesse considerazioni per un pesce che si trova nella casella in alto
                            
                            if(ageshark[x][y]==agereproductionshark){
                                world[x][y]=2;
                                ageshark[x][y]=0;
                                noeat[x][y]=0;
                            }else world[x][y]=0;
                            
                            if(x-1>L-1){
                                world[0][y]=2;
                                alreadymovedshark[0][y]=1;
                                agemin[0][y]=0;
                                ageshark[0][y]=ageshark[x][y]+1;
                                noeat[0][y]=0;
                            }
                            else if(x-1<0){
                                world[L-1][y]=2;
                                alreadymovedshark[L-1][y]=1;
                                agemin[L-1][y]=0;
                                ageshark[L-1][y]=ageshark[x][y]+1;
                                noeat[L-1][y]=0;
                            }
                            else{
                                world[x-1][y]=2;
                                alreadymovedshark[x-1][y]=1;
                                agemin[x-1][y]=0;
                                ageshark[x-1][y]=ageshark[x][y]+1;
                                noeat[x-1][y]=0;
                            }
                            
                            if(world[x][y]==2) ageshark[x][y]=1;
                            else{
                                ageshark[x][y]=0;
                                noeat[x][y]=0;
                            }
                            
                            flag=1;
                        }else if(rnd1==rightmin){ //Stesse considerazioni per un pesce che si trova nella casella a destra
                            
                            if(ageshark[x][y]==agereproductionshark){
                                world[x][y]=2;
                                ageshark[x][y]=0;
                                noeat[x][y]=0;
                            }else world[x][y]=0;
                            
                            if(y+1>D-1){
                                world[x][0]=2;
                                alreadymovedshark[x][0]=1;
                                agemin[x][0]=0;
                                ageshark[x][0]=ageshark[x][y]+1;
                                noeat[x][0]=0;
                            }
                            else if(y+1<0){
                                world[x][D-1]=2;
                                alreadymovedshark[x][D-1]=1;
                                agemin[x][D-1]=0;
                                ageshark[x][D-1]=ageshark[x][y]+1;
                                noeat[x][D-1]=0;
                            }
                            else{
                                world[x][y+1]=2;
                                alreadymovedshark[x][y+1]=1;
                                agemin[x][y+1]=0;
                                ageshark[x][y+1]=ageshark[x][y]+1;
                                noeat[x][y+1]=0;
                            }
                            
                            if(world[x][y]==2) ageshark[x][y]=1;
                            else{
                                ageshark[x][y]=0;
                                noeat[x][y]=0;
                            }
                            
                            flag=1;
                        }else if(rnd1==leftmin){ //Stesse considerazioni per un pesce che si trova nella casella a sinistra
                            
                            if(ageshark[x][y]==agereproductionshark){
                                world[x][y]=2;
                                ageshark[x][y]=0;
                                noeat[x][y]=0;
                            }else world[x][y]=0;
                            
                            if(y-1>D-1){
                                world[x][0]=2;
                                alreadymovedshark[x][0]=1;
                                agemin[x][0]=0;
                                ageshark[x][0]=ageshark[x][y]+1;
                                noeat[x][0]=0;
                            }
                            else if(y-1<0){
                                world[x][D-1]=2;
                                alreadymovedshark[x][D-1]=1;
                                agemin[x][D-1]=0;
                                ageshark[x][D-1]=ageshark[x][y]+1;
                                noeat[x][D-1]=0;
                            }
                            else{
                                world[x][y-1]=2;
                                alreadymovedshark[x][y-1]=1;
                                agemin[x][y-1]=0;
                                ageshark[x][y-1]=ageshark[x][y]+1;
                                noeat[x][y-1]=0;
                            }
                            
                            if(world[x][y]==2) ageshark[x][y]=1;
                            else{
                                ageshark[x][y]=0;
                                noeat[x][y]=0;
                            }
                            
                            flag=1;
                        }
                        
                        ao++; //Contatore di controllo per evitare che il ciclo do while vada in loop
                        if(ao>200){
                            printf("FAIL %d\n", ao);
                            break;
                        }
                        
                    }while(flag==0);
                    
//                    stampa(world);
//                    printf("noeat\n");
//                    stampa(noeat);
                    
                }else if(downmin==0 && upmin==0 && rightmin==0 && leftmin==0){ //Caso in cui non ci sono pesci nelle vicinanze
                    
                    if(noeat[x][y]>agedeathshark){ //Lo squalo ha raggiunto un'età di digiuno superiore a quella massima
                        world[x][y]=0; //Lo squalo muore
                        ageshark[x][y]=0; //Si azzerano le quantità di interesse
                        noeat[x][y]=0;
                    }else{ //Se lo squalo non ha raggiunto età di digiuno limite
                        
                        if(down!=0 || up!=0 || right!=0 || left!=0){ //Caso in cui almeno una casella adiacente è libera
                            
                            do{
                                rnd1=rndom(1, 4); //Mi muovo a caso in una direzione
                                
                                if(rnd1==down){
                                    
                                    if(ageshark[x][y]==agereproductionshark){ //Se la squalo ha raggiunto età di riproduzione
                                        world[x][y]=2; //La casella che lascio viene popolata
                                        ageshark[x][y]=0; //Per ora il figlio ha età nulla
                                    }else world[x][y]=0; //Se non si riproduce mi muovo in modo classico
                                    
                                    if(x+1>L-1){ //PBC
                                        world[0][y]=2; //Popolo la casella in cui mi sposto
                                        alreadymovedshark[0][y]=1; //Registro che questo squalo è già stato mosso in questo ciclo
                                        ageshark[0][y]=ageshark[x][y]+1; //Aumento di un'unità l'eta dello squalo
                                        noeat[0][y]=noeat[x][y]+1; //Aumento di un'unità l'età di digiuno dello squalo
                                    }
                                    else if(x+1<0){ //PBC
                                        world[L-1][y]=2;
                                        alreadymovedshark[L-1][y]=1;
                                        ageshark[L-1][y]=ageshark[x][y]+1;
                                        noeat[L-1][y]=noeat[x][y]+1;
                                    }
                                    else{ //PBC
                                        world[x+1][y]=2;
                                        alreadymovedshark[x+1][y]=1;
                                        ageshark[x+1][y]=ageshark[x][y]+1;
                                        noeat[x+1][y]=noeat[x][y]+1;
                                    }
                                    
                                    noeat[x][y]=0; //Azzero l'età di digiuno nella casella che lascio
                                    
                                    if(world[x][y]==2) ageshark[x][y]=1; //Se lo squalo si riproduce, l'età del figlio è 1
                                    else ageshark[x][y]=0; //Altrimenti azzero l'età nella casella che lascio
                                    
                                    flag=1; //Contatore per uscire dal ciclo do while
                                }else if(rnd1==up){ //Stesse considerazioni per movimento verso l'alto
                                    
                                    if(ageshark[x][y]==agereproductionshark){
                                        world[x][y]=2;
                                        ageshark[x][y]=0;
                                    }else world[x][y]=0;
                                    
                                    if(x-1>L-1){
                                        world[0][y]=2;
                                        alreadymovedshark[0][y]=1;
                                        ageshark[0][y]=ageshark[x][y]+1;
                                        noeat[0][y]=noeat[x][y]+1;
                                    }
                                    else if(x-1<0){
                                        world[L-1][y]=2;
                                        alreadymovedshark[L-1][y]=1;
                                        ageshark[L-1][y]=ageshark[x][y]+1;
                                        noeat[L-1][y]=noeat[x][y]+1;
                                    }
                                    else{
                                        world[x-1][y]=2;
                                        alreadymovedshark[x-1][y]=1;
                                        ageshark[x-1][y]=ageshark[x][y]+1;
                                        noeat[x-1][y]=noeat[x][y]+1;
                                    }
                                    
                                    noeat[x][y]=0;
                                    
                                    if(world[x][y]==2) ageshark[x][y]=1;
                                    else ageshark[x][y]=0;
                                    
                                    flag=1;
                                }else if(rnd1==right){ //Stesse considerazioni per movimento verso destra
                                    
                                    if(ageshark[x][y]==agereproductionshark){
                                        world[x][y]=2;
                                        ageshark[x][y]=0;
                                    }else world[x][y]=0;
                                    
                                    if(y+1>D-1){
                                        world[x][0]=2;
                                        alreadymovedshark[x][0]=1;
                                        ageshark[x][0]=ageshark[x][y]+1;
                                        noeat[x][0]=noeat[x][y]+1;
                                    }
                                    else if(y+1<0){
                                        world[x][D-1]=2;
                                        alreadymovedshark[x][D-1]=1;
                                        ageshark[x][D-1]=ageshark[x][y]+1;
                                        noeat[x][D-1]=noeat[x][y]+1;
                                    }
                                    else{
                                        world[x][y+1]=2;
                                        alreadymovedshark[x][y+1]=1;
                                        ageshark[x][y+1]=ageshark[x][y]+1;
                                        noeat[x][y+1]=noeat[x][y]+1;
                                    }
                                    
                                    noeat[x][y]=0;
                                    
                                    if(world[x][y]==2) ageshark[x][y]=1;
                                    else ageshark[x][y]=0;
                                    
                                    flag=1;
                                }else if(rnd1==left){ //Stesse considerazioni per movimento verso sinistra
                                    
                                    if(ageshark[x][y]==agereproductionshark){
                                        world[x][y]=2;
                                        ageshark[x][y]=0;
                                    }else world[x][y]=0;
                                    
                                    if(y-1>D-1){
                                        world[x][0]=2;
                                        alreadymovedshark[x][0]=1;
                                        ageshark[x][0]=ageshark[x][y]+1;
                                        noeat[x][0]=noeat[x][y]+1;
                                    }
                                    else if(y-1<0){
                                        world[x][D-1]=2;
                                        alreadymovedshark[x][D-1]=1;
                                        ageshark[x][D-1]=ageshark[x][y]+1;
                                        noeat[x][D-1]=noeat[x][y]+1;
                                    }
                                    else{
                                        world[x][y-1]=2;
                                        alreadymovedshark[x][y-1]=1;
                                        ageshark[x][y-1]=ageshark[x][y]+1;
                                        noeat[x][y-1]=noeat[x][y]+1;
                                    }
                                    
                                    noeat[x][y]=0;
                                    
                                    if(world[x][y]==2) ageshark[x][y]=1;
                                    else ageshark[x][y]=0;
                                    
                                    flag=1;
                                }
                                
                                ao++; //Contatore di controllo per evitare che il do while vada in loop
                                if(ao>200){
                                    printf("FAIL %d\n", ao);
                                    break;
                                }
                                
                            }while(flag==0);
                            
//                            stampa(world);
//                            printf("noeat\n");
//                            stampa(noeat);
                            
                        }else if(down==0 && up==0 && right==0 && left==0){ //Caso in cui lo squalo non ha possibilità di movimento perchè circondato da altri squali. Se ha raggiunto l'età di riproduzione in questo caso si riproduce e il genitore muore.
                            
                            if(noeat[x][y]>agedeathshark){ //Lo squalo ha raggiunto un'età di digiuno superiore a quella massima
                                world[x][y]=0; //Lo squalo muore
                                ageshark[x][y]=0; //Si azzerano le quantità di interesse
                                noeat[x][y]=0;
                            }else{
                                ageshark[x][y]=ageshark[x][y]+1; //Aumento l'età dello squalo di un'unità
                                noeat[x][y]=noeat[x][y]+1; //Aumento l'età di digiuno dello squalo di un'unità
                            }
                            
//                            stampa(world);
//                            printf("noeat\n");
//                            stampa(noeat);
                        }
                        
                    }
                    
                    
                }
                
            }
            
            
        }
    }
    
    
}

void population(int world[L][D], int *nshark, int *nminnow){

    int x, y;
    
    for(x=0; x<L; x++){
        for(y=0; y<D; y++){
            
            if(world[x][y]==1) *nminnow+=1;
            else if(world[x][y]==2) *nshark+=1;
            
        }
    }
    
    
}

void stampa(int world[L][D]){
    
    int x, y;
    
    for(x=0; x<L; x++){
        for(y=0; y<D; y++){
            
            printf("%d ", world[x][y]);
            
        }
        printf("\n");
    }
    
    printf("\n\n");
}

void gif(int world[L][D], FILE *fn1, FILE *fn2, FILE *fn3){
    
    int x, y;

    for(x=0; x<L; x++){
        
        for(y=0; y<D; y++){
            
            if(world[x][y]==1) fprintf(fn1, "%d %d\n", x+1, y+1);
            else if(world[x][y]==2) fprintf(fn2, "%d %d\n", x+1, y+1);
            else fprintf(fn3, "%d %d\n", x+1, y+1);
            
        }
        
    }
    fprintf(fn1, "\n\n");
    fprintf(fn2, "\n\n");
    fprintf(fn3, "\n\n");
    
}

void nuclear(int world[L][D], int agemin[L][D], int lmax, int lmin, int dmax, int dmin, double mortalita){
    
    int x, y;
    double prob;
    
    
    for(x=lmin; x<=lmax; x++){
        for(y=dmin; y<=dmax; y++){
            
            if(world[x][y]==1){
                
                prob=(double)lrand48()/RAND_MAX;
                
                if(prob<=mortalita){
                    world[x][y]=0;
                    agemin[x][y]=0;
                }
            }
            
        }
    }
    
    
}
